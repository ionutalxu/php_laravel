<!DOCTYPE html>
<!--
 Escribe un programa PHP que realice lo siguiente: declarar dos variables X e 
Y de tipo int, dos variables N y M de tipo double y asigna a cada una un valor. 
A continuación muestra por pantalla:
    • El valor de cada variable.
    • La suma X + Y
    • La diferencia X – Y
    • El producto X * Y
    • El cociente X / Y
    • El resto X % Y
    • La suma N + M
    • La diferencia N – M
    • El producto N * M
    • El cociente N / M
    • El resto N % M
    • La suma X + N
    • El cociente Y / M
    • El resto Y % M
    • El doble de cada variable
    • La suma de todas las variables
    • El producto de todas las variables
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>S1_6</title>
    </head>
    <body>
        <?php
        // Declaración de variables
        $varX = 25;
        $varY = 10;
        $varN = 32.69;
        $varM = 74.21;

        echo "Variable X = $varX";
        echo "<br>";
        echo "Variable Y = $varY";
        echo "<br>";
        echo "Variable N = $varN";
        echo "<br>";
        echo "Variable M = $varM";
        echo "<br>";
        echo "<br>";

        // Operaciones con las variables X, Y
        echo "Suma de $varX(X) + $varY(Y) = " . ($varX + $varY);
        echo "<br>";
        echo "Diferencia de $varX(X) - $varY(Y) = " . ($varX - $varY);
        echo "<br>";
        echo "Producto de $varX(X) * $varY(Y) = " . ($varX * $varY);
        echo "<br>";
        echo "Cociente de $varX(X) / $varY(Y) = " . ($varX / $varY);
        echo "<br>";
        echo "Resto de $varX(X) % $varY(Y) = " . ($varX % $varY);
        echo "<br>";
        echo "<br>";

        // Operaciones con las variables N, M
        $sumaNM = $varN + $varM;
        $restaNM = $varN - $varM;
        $productoNM = $varN * $varM;
        $cocienteNM = $varN / $varM;
        $restoNM = $varN % $varM;

        echo "Suma de $varN(N) + $varM(M) = $sumaNM";
        echo "<br>";
        echo "Diferencia de $varN(N) - $varM(M) = $restaNM";
        echo "<br>";
        echo "Producto de $varN(N) * $varM(M) = $productoNM";
        echo "<br>";
        echo "Cociente de $varN(N) / $varM(M) = $cocienteNM";
        echo "<br>";
        echo "Resto de $varN(N) % $varM(M) = $restoNM";
        echo "<br>";
        echo "<br>";

        echo "Suma de $varX(X) + $varN(N) = " . ($varX + $varN);
        echo "<br>";
        echo "Cociente de $varY(Y) + $varM(M) = " . ($varY / $varM);
        echo "<br>";
        echo "Resto de $varY(Y) + $varM(M) = " . ($varY % $varM);
        echo "<br>";
        echo "<br>";

        echo "El doble de cada número";
        echo "<br>";
        echo "$varX * 2 = " . ($varX * 2);
        echo "<br>";
        echo "$varY * 2 = " . ($varY * 2);
        echo "<br>";
        echo "$varN * 2 = " . ($varN * 2);
        echo "<br>";
        echo "$varM * 2 = " . ($varM * 2);
        echo "<br>";
        echo "<br>";

        echo "La suma de todas las variables ";
        echo "$varX + $varY + $varN + $varM = ";
        $sumaTotal = $varX + $varY + $varN + $varM;
        echo $sumaTotal;
        echo "<br>";

        echo "El producto de todas las variables ";
        echo "$varX * $varY * $varN * $varM = ";
        $productoTotal = $varX * $varY * $varN * $varM;
        echo $productoTotal;

        ?>
    </body>
</html>
