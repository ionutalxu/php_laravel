<!DOCTYPE html>
<!--
Generar un valor aleatorio entre 1 y 3. Luego imprimir en castellano el número
(Ej. si se genera el 3 luego mostrar en la página el string "tres").
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>S1_3</title>
    </head>
    <body>
        <?php
        // Genero un número aleatorio entre el 1 y el 3
        $num = random_int(1, 3);
        
        // Switch para cada uno de los casos
        switch($num) {
            case 1:
                echo "uno";
                break;
            case 2:
                echo "dos";
                break;
            case 3:
                echo "tres";
                break;
        }
        ?>
    </body>
</html>
