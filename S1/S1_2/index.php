<!DOCTYPE html>
<!--
Definir tres variables enteras. Luego definir un string que incorpore dichas
variables y las sustituya en tiempo de ejecución
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>S1_2</title>
    </head>
    <body>
        <?php
        // Variables enteras
        $var1 = 16;
        $var2 = 2;
        $var3 = 96;
        
        $varString = "Las variables anteriores son $var1, $var2 y $var3";
        
        echo $varString;
        
        ?>
    </body>
</html>
