<!DOCTYPE html>
<!--
Escribe un programa PHP que declare una variable entera N y asígnale un valor. 
A continuación escribe las instrucciones que realicen los siguientes:
    • Incrementar N en 77.
    • Decrementarla en 3.
    • Duplicar su valor.
Si por ejemplo N = 1 la salida del programa será:
    • Valor inicial de N = 1
    • N + 77 = 78
    • N - 3 = 75
    • N * 2 = 150
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>S1_7</title>
    </head>
    <body>
        <?php
        $varN = 1;
        echo "Valor inicial de N = $varN";
        echo "<br>";
        
        $varN += 77;
        echo "N + 77 = $varN";
        echo "<br>";
        
        $varN -= 3;
        echo "N - 3 = $varN";
        echo "<br>";
        
        $varN *= 2;
        echo "N * 2 = $varN";
        
        
        
        ?>
    </body>
</html>
