<!DOCTYPE html>
<!--
1) Definir una variable de cada tipo: integer, double, string y boolean. Luego
imprimirlas en la página, una por línea.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>S1_1</title>
    </head>
    <body>
        <?php
        // Variable integer
        $numero = 16;
        echo $numero;
        echo "<br>";
        
        // Variable double
        $decimal = 16.96;
        echo $decimal;
        echo "<br>";
        
        // Variable string
        $fecha = "16 de febrero";
        echo $fecha . "<br>";
        
        // Variable boolean
        $variableB = true;
        echo $variableB;
        echo "<br>";
        
        ?>
    </body>
</html>
