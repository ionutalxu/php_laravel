<!DOCTYPE html>
<!--
Escribe un programa PHP que realice lo siguiente: declarar una variable N de 
tipo int, una variable A de tipo double y una variable C de tipo char y 
asigna a cada una un valor.
A continuación muestra por pantalla:
• El valor de cada variable.
• La suma de N + A
• La diferencia de A - N
• El valor numérico correspondiente al carácter que contiene la variable C. 
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>S1_5</title>
    </head>
    <body>
        <?php
        $varN = 6;
        $varA = 16.2;
        $varC = 'a';
        
        // Operaciones
        $suma = $varN + $varA;
        $resta = $varA - $varN;
        // Código ASCII del char
        $valorNumerico = ord($varC);
        
        echo "Variable N = $varN";
        echo "<br>";
        echo "Variable A = $varA";
        echo "<br>";
        echo "Variable C = $varC";
        echo "<br>";
        echo "$varN + $varA = $suma";
        echo "<br>";
        echo "$varA - $varN = $resta";
        echo "<br>";
        echo "Valor numérico del carácter $varC = $valorNumerico";
        
        ?>
    </body>
</html>
