<!DOCTYPE html>
<!--
 Declara dos variables numéricas (con el valor que desees), muestra por 
pantalla la suma, resta, multiplicación, división y módulo (resto)
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>S1_4</title>
    </head>
    <body>
        <?php
        // Declaración de variables
        $num1 = 16;
        $num2 = 3;
        
        echo "<h3>Operaciones</h3>";
        
        // Operaciones
        echo "Suma: $num1 + $num2 = " . ($num1 + $num2) . "<br>";
        
        echo "Resta: $num1 - $num2 = ";
        echo $num1 - $num2;
        echo "<br>";
        
        $multiplicacion = $num1 * $num2;
        echo "Multiplicación: $num1 * $num2 = $multiplicacion";
        echo "<br>";
        
        echo "División: $num1 / $num2 = " . ($num1 / $num2);
        echo "<br>";
        
        echo "Módulo: $num1 % $num2 = ";
        echo $num1 % $num2;
        ?>
    </body>
</html>
