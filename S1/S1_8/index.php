<!DOCTYPE html>
<!--
Programa PHP que declare cuatro variables enteras A, B, C y D y asígnale un valor a cada
una. A continuación realiza las instrucciones necesarias para que:
    • B tome el valor de C
    • C tome el valor de A
    • A tome el valor de D
    • D tome el valor de B
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>S1_8</title>
    </head>
    <body>
        <?php
        // Declaración de variables
        $varA = 1;
        $varB = 2;
        $varC = 3;
        $varD = 4;

        // Muestro el valor original de las variables
        echo "<h2>Valor original de las variables:</h2>";
        echo "A: $varA";
        echo "<br/>";
        echo "B: $varB";
        echo "<br/>";
        echo "C: $varC";
        echo "<br/>";
        echo "D: $varD";
        echo "<br/>";

        // Cambio su valor y las imprimo de nuevo
        $auxvarB = $varC;
        $auxvarC = $varA;
        $auxvarA = $varD;
        $auxvarD = $varB;

        echo "<h2>Valor nuevo de las variables:</h2>";
        echo "A: $auxvarA";
        echo "<br/>";
        echo "B: $auxvarB";
        echo "<br/>";
        echo "C: $auxvarC";
        echo "<br/>";
        echo "D: $auxvarD";
        echo "<br/>";
        ?>
    </body>
</html>
